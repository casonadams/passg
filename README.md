# passg

A password generator that is designed to generate pronounceable
yet length flexible passwords.

## using with gopass

- setup [gopass](https://www.gopass.pw/)
- `cargo install --path=.`

```bash
GOPASS_EXTERNAL_PWGEN="passg -p -l" gopass generate -g external test.org/me
```

```bash
cargo run

------------------------
Eat-She-Tore-Map7-Passes
------------------------
score:      4
length:     24
crack time: 57 years
```

## Building

```bash
cargo build
```

## Development

```bash
cargo run -- --help
```

## ci / cd

```bash
docker build -t casonadams/gitlab-rust .
docker push casonadams/gitlab-rust
```
