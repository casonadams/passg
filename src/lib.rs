use anyhow::Result;

pub mod dict;
pub mod generator;
pub mod print;
pub mod utils;

pub fn run(separator: String, length: usize, hidden: bool) -> Result<String> {
  let password = generator::passwd(separator, length)?;
  print::print(password.clone(), hidden);
  Ok(password)
}
