use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
use zxcvbn::zxcvbn;

pub fn print(password: String, hidden: bool) {
  if hidden {
    print!("{}", password);
    return;
  }

  let entropy = zxcvbn(&password, &[]).unwrap();
  let mut stdout = StandardStream::stdout(ColorChoice::Always);

  // generate dynamic header and footer around password
  let sep = String::from_utf8(vec![b'-'; password.len()]).unwrap();
  println!("{}", sep);
  println!("{}", password);
  println!("{}", sep);

  let mut color = Color::Green;
  let _ = write!(&mut stdout, "score:      ");
  if entropy.score() < 4 {
    color = Color::Red
  }
  stdout.set_color(ColorSpec::new().set_fg(Some(color))).unwrap();
  let _ = writeln!(&mut stdout, "{}", entropy.score());
  StandardStream::stdout(ColorChoice::Always).reset().unwrap();

  let _ = writeln!(&mut stdout, "length:     {}", password.len());
  let _ = writeln!(
    &mut stdout,
    "crack time: {}",
    entropy.crack_times().offline_fast_hashing_1e10_per_second().to_string()
  );
  StandardStream::stdout(ColorChoice::Always).reset().unwrap();
}
