use crate::dict::Word;
use anyhow::Result;
use rand::seq::SliceRandom;
use rand::thread_rng;
use rand::Rng;

pub fn passwd(separator: String, length: usize) -> Result<String> {
  // word size options for password
  let sizes: Vec<usize> = vec![4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11];

  let mut password = vec![];
  let mut c = Combos::default();
  c.get_combs(sizes, length, &[]);
  let mut rng = thread_rng();

  let bytes = include_bytes!("../share/dict/words");
  let rand: usize = rng.gen_range(0..c.combos.len());
  let num_location: usize = rng.gen_range(0..c.combos[rand].len());
  c.combos[rand].shuffle(&mut rng);

  for (i, word) in c.combos[rand].clone().into_iter().enumerate() {
    // word - 1 is so we can add a sperator and number
    let mut w = Word::get_word(word - 1, false, bytes)?.word;
    if i == num_location {
      w.push_str(&rng.gen_range(0..10).to_string());
    }
    password.push(w);
  }

  Ok(password.join(&separator))
}

#[derive(Debug, Default)]
struct Combos {
  combos: Vec<Vec<usize>>,
}

impl Combos {
  fn get_combs(&mut self, numbers: Vec<usize>, target: usize, partial: &[usize]) {
    let s: usize = partial.iter().sum();

    if s == target {
      self.combos.push(partial.to_vec());
      self.combos.dedup();
    }
    if s >= target {
      return;
    }

    for (i, _) in numbers.to_owned().into_iter().enumerate() {
      let n = numbers[i] as usize;
      let remaining: Vec<usize> = numbers[i + 1..].into();
      let mut p = partial.to_owned();
      p.push(n);
      self.get_combs(remaining, target, &p);
    }
  }
}
