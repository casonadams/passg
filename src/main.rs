use structopt::StructOpt;

#[derive(Debug, StructOpt, Clone)]
#[structopt(name = "passg", about = "A simple way to create easy to type strong passwords")]
pub struct Opt {
  /// Password length
  #[structopt(short, long, default_value = "30")]
  length: usize,
  /// Separator between groups
  #[structopt(short = "s", long = "separator", default_value = "-")]
  separator: String,
  /// Hides password stats
  #[structopt(short, long)]
  protected: bool,
}

fn main() {
  let opt = Opt::from_args();

  if opt.length < 4 {
    eprintln!("Passwords must be at least 4 chars in length");
    std::process::exit(1);
  }
  if opt.length > 112 {
    eprintln!("Passwords must not be larger than 112 chars in length");
    std::process::exit(1);
  }

  let _ = passwd_gen::run(opt.separator, opt.length, opt.protected);
}
