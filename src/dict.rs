use anyhow::{anyhow, Result};
use convert_case::{Case, Casing};
use rand::Rng;
use regex::Regex;
use std::collections::HashSet;

// return words of length 1 - 24
fn get_words_of_len(size: usize, bytes: &[u8]) -> Result<Vec<String>> {
  let mut v = Vec::new();

  // TODO figure out a better way to build the regex dynamicly
  // when using the list from os dictionary this needs to filter
  let mut s = "".to_string();
  let re_s = "\\w".to_string();
  for _ in 0..size {
    s += &re_s.to_string();
  }
  let re = Regex::new(&format!("(?m)^{}$", s)).unwrap();

  for s in re.captures_iter(std::str::from_utf8(bytes)?) {
    v.push(s[0].to_owned());
  }
  Ok(v)
}

/// A Word from a list is represented here
#[derive(Debug, Clone)]
pub struct Word {
  /// A random word from a list
  pub word: String,
  /// List size word came from
  pub list_size: usize,
  /// List of letters in set
  pub letters: HashSet<char>,
}

impl Word {
  /// Returns a random word and the size of the list it came from
  ///
  /// # Arguments
  ///
  /// * `length` - the character count of a word
  /// * `caps` - if the word should randomly be all UPPERCASE
  pub fn get_word(length: usize, caps: bool, bytes: &[u8]) -> Result<Word> {
    let mut rng = rand::thread_rng();
    let words = get_words_of_len(length, bytes)?;

    let mut letters: HashSet<char> = HashSet::new();
    let list_size = words.len();
    if list_size != 0 {
      let mut word: String = (words[rng.gen_range(0..list_size)]).parse()?;

      if !caps {
        word = word.to_case(Case::Title);
      } else {
        word = word.to_lowercase();
      }

      for w in &words {
        for letter in w.chars() {
          let l = letter.to_string();
          letters.insert(l.parse()?);
        }
      }
      Ok(Word {
        word,
        list_size,
        letters,
      })
    } else {
      Err(anyhow!("no words found"))
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_get_words() {
    let bytes = include_bytes!("../share/dict/words");
    for s in 1..25 {
      for w in get_words_of_len(s, bytes).unwrap() {
        assert_eq!(w.len(), s);
      }
    }
  }

  #[test]
  fn test_get_word() {
    let bytes = include_bytes!("../share/dict/words");
    for s in 1..26 {
      if let Ok(w) = Word::get_word(s, false, bytes) {
        let len = w.word.len();
        let list = w.list_size;
        assert_eq!(len, s);
        assert_ne!(list, 0);
      };
    }
  }
}
