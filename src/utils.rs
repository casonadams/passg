use rand::Rng;
pub fn digit_pad(mut rng: rand::rngs::ThreadRng, size: i32) -> String {
  (0..size).map(|_| rng.gen_range(0..9).to_string()).collect::<String>()
}
