FROM registry.fedoraproject.org/fedora-minimal:35

RUN microdnf install -y \
  gcc \
  openssl-devel \
  && microdnf clean all \
  ;

ENV RUSTUP_HOME=/usr/local/rustup \
  CARGO_HOME=/usr/local/cargo \
  CARGO_TARGET_DIR=/usr/local/target \
  PATH=/usr/local/cargo/bin:$PATH

RUN curl -f -L https://static.rust-lang.org/rustup.sh -O \
  && sh rustup.sh -y \
  --no-modify-path \
  --profile minimal \
  && rm rustup.sh \
  ;
